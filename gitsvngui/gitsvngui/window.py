'''
Created on Jun 8, 2012

@author: Martin Jakl
'''

from gi.repository import Gtk, GObject, Pango
import configparser
import os
from .git import git


class MainWindow(Gtk.Window):
    def __init__(self):
        icon = None
        try:
            icon = Gtk.IconTheme.get_default().load_icon("git", 64, 0)
        except:
            pass
        Gtk.Window.__init__(self, title="Git SVN GUI", icon=icon)

        self.config_path = os.environ["HOME"] \
            + "/.config/qbicon/gitsvngui/config"

        grid = Gtk.Grid.new()
        grid.set_size_request(800, 600)
        grid.set_border_width(10)
        self.add(grid)

        addButton = Gtk.ToolButton.new_from_stock(Gtk.STOCK_ADD)
        addButton.connect("clicked", self.on_add_button_clicked)
        remButton = Gtk.ToolButton.new_from_stock(Gtk.STOCK_REMOVE)
        remButton.connect("clicked", self.on_remove_button_clicked)
        addAllButton = Gtk.ToolButton.new_from_stock(Gtk.STOCK_FIND)
        addAllButton.connect("clicked", self.on_addall_button_clicked)
        refreshButton = Gtk.ToolButton.new_from_stock(Gtk.STOCK_REFRESH)
        refreshButton.connect("clicked", self.on_refresh_button_clicked)
        upButton = Gtk.ToolButton.new_from_stock(Gtk.STOCK_GO_UP)
        upButton.connect("clicked", self.on_up_button_clicked)
        downButton = Gtk.ToolButton.new_from_stock(Gtk.STOCK_GO_DOWN)
        downButton.connect("clicked", self.on_down_button_clicked)
        topBox = Gtk.Grid.new()
        topBox.add(addButton)
        topBox.add(remButton)
        topBox.add(addAllButton)
        topBox.add(refreshButton)
        topBox.add(upButton)
        topBox.add(downButton)

        svnfetchButton = Gtk.Button.new_with_label("Fetch SVN")
        svnfetchButton.connect("clicked", self.on_fetchsvn_button_clicked)
        svnrebaseButton = Gtk.Button.new_with_label("Rebase SVN")
        svnrebaseButton.connect("clicked", self.on_rebasesvn_button_clicked)
        svndcommitButton = Gtk.Button.new_with_label("Dcommit SVN")
        svndcommitButton.connect("clicked", self.on_dcommitsvn_button_clicked)
        svninfoButton = Gtk.Button.new_with_label("Info SVN")
        svninfoButton.connect("clicked", self.on_infosvn_button_clicked)
        pullButton = Gtk.Button.new_with_label("Pull")
        pullButton.connect("clicked", self.on_pull_button_clicked)
        pushButton = Gtk.Button.new_with_label("Push")
        pushButton.connect("clicked", self.on_push_button_clicked)
        statusButton = Gtk.Button.new_with_label("Status")
        statusButton.connect("clicked", self.on_status_button_clicked)
        gcButton = Gtk.Button.new_with_label("GC")
        gcButton.connect("clicked", self.on_gc_button_clicked)
        cleanButton = Gtk.Button.new_with_label("Clean")
        cleanButton.connect("clicked", self.on_clean_button_clicked)
        gitkButton = Gtk.Button.new_with_label("GitK")
        gitkButton.connect("clicked", self.on_gitk_button_clicked)
        gitkallButton = Gtk.Button.new_with_label("GitK All")
        gitkallButton.connect("clicked", self.on_gitkall_button_clicked)
        gitguiButton = Gtk.Button.new_with_label("Git GUI")
        gitguiButton.connect("clicked", self.on_gitgui_button_clicked)
        termButton = Gtk.Button.new_with_label("Terminal")
        termButton.connect("clicked", self.on_term_button_clicked)
        stashButton = Gtk.Button.new_with_label("Stash")
        stashButton.connect("clicked", self.on_stash_button_clicked)
        stashpopButton = Gtk.Button.new_with_label("Stash Pop")
        stashpopButton.connect("clicked", self.on_stashpop_button_clicked)
        sideBox = Gtk.Grid()
        sideBox.set_orientation(Gtk.Orientation.VERTICAL)
        sideBox.add(svnfetchButton)
        sideBox.add(svnrebaseButton)
        sideBox.add(svndcommitButton)
        sideBox.add(svninfoButton)
        sideBox.add(pullButton)
        sideBox.add(pushButton)
        sideBox.add(statusButton)
        sideBox.add(gcButton)
        sideBox.add(cleanButton)
        sideBox.add(gitkButton)
        sideBox.add(gitkallButton)
        sideBox.add(gitguiButton)
        sideBox.add(termButton)
        sideBox.add(stashButton)
        sideBox.add(stashpopButton)

        self.comboStore = Gtk.ListStore.new([str])
        self.repo_list = Gtk.ListStore.new([str, str, str, int])
        self.tree = Gtk.TreeView.new()
        self.tree.set_model(self.repo_list)
        rend = Gtk.CellRendererText.new()
        comb = Gtk.CellRendererCombo.new()
        comb.set_property("editable", True)
        comb.set_property("model", self.comboStore)
        comb.set_property("text-column", 0)
        comb.set_property("has-entry", False)
        comb.connect("edited", self.on_combo_changed)
        comb.connect("editing-started", self.on_combo_selected)
        self.tree.append_column(Gtk.TreeViewColumn("Repositories",
                                                   rend, text=0))
        self.tree.append_column(Gtk.TreeViewColumn("Type", rend, text=1))
        self.tree.append_column(Gtk.TreeViewColumn("Branch", comb, text=2))
        self.tree.append_column(Gtk.TreeViewColumn("Branch count", rend,
                                                   text=3))
        self.tree.set_headers_visible(False)
        self.tree.get_selection().set_mode(Gtk.SelectionMode.MULTIPLE)
        scroll = Gtk.ScrolledWindow()
        scroll.set_vexpand(True)
        scroll.set_hexpand(True)
        scroll.add(self.tree)

        self.output = Gtk.TextView()
        self.output.set_editable(False)
        self.red = self.output.get_buffer().create_tag(None, foreground="red")
        self.bold = self.output.get_buffer() \
            .create_tag(None, weight=Pango.Weight.BOLD)
        self.output.set_cursor_visible(False)
        scroll1 = Gtk.ScrolledWindow()
        scroll1.set_vexpand(True)
        scroll1.set_hexpand(True)
        scroll1.add(self.output)

        pan = Gtk.Paned()
        pan.set_orientation(Gtk.Orientation.VERTICAL)
        pan.add(scroll)
        pan.add(scroll1)
        pan.set_position(400)

        grid.add(pan)
        grid.attach_next_to(topBox, pan, Gtk.PositionType.TOP, 1, 1)
        grid.attach_next_to(sideBox, pan, Gtk.PositionType.RIGHT, 1, 1)

        statusline = Gtk.Statusbar()
        git.statusline = statusline
        git.context = statusline.get_context_id("test")
        grid.attach_next_to(statusline, pan, Gtk.PositionType.BOTTOM, 1, 1)

        self.read_configuration()
        GObject.timeout_add(5000, self._refresh)

    def _refresh(self):
        for repo in self.repo_list:
            g = git(repo[0], self)
            cur, branch = g.get_branches()
            if branch[cur] != repo[2]:
                repo[2] = branch[cur]
                repo[3] = len(branch)
        return True

    def on_combo_changed(self, widget, path, text):
        name = self.repo_list[path][0]
        g = git(name, self)
        if text is not None and text != self.repo_list[path][2]:
            g.checkout(text)
            self._refresh()

    def on_combo_selected(self, widget, box, path):
        name = self.repo_list[path][0]
        g = git(name, self)
        _, br = g.get_branches()
        self.comboStore.clear()
        for b in br:
            self.comboStore.append([b])

    def on_add_button_clicked(self, widget):
        dlg = Gtk.FileChooserDialog("Select git repository",
                                    self, Gtk.FileChooserAction.SELECT_FOLDER,
                                    (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                     Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        resp = dlg.run()
        if resp == Gtk.ResponseType.OK:
            name = dlg.get_filename()
            if os.path.exists(name + "/.git"):
                self._add_repo(name)
                self.write_configuration()
            else:
                self.show_error("Not git repository.")
        dlg.destroy()

    def on_remove_button_clicked(self, widget):
        (mod, paths) = self.tree.get_selection().get_selected_rows()
        iters = []
        for path in paths:
            iters.append(mod.get_iter(path))
        for it in iters:
            mod.remove(it)
        self.write_configuration()

    def on_addall_button_clicked(self, widget):
        curr = set()
        for c in self.repo_list:
            curr.add(c[0])
        repos = git.find_all_repos(self)
        for r in repos:
            if r not in curr:
                self._add_repo(r)
        self.write_configuration()

    def on_refresh_button_clicked(self, widget):
        self._refresh()

    def on_up_button_clicked(self, widget):
        (mod, it) = self.tree.get_selection().get_selected_rows()
        if len(it) == 1:
            p = it[0]
            i = mod.get_iter(p)
            if i != mod.get_iter_first():
                p.prev()
                mod.swap(i, mod.get_iter(p))
                self.tree.scroll_to_cell(p)
                self.write_configuration()

    def on_down_button_clicked(self, widget):
        (mod, it) = self.tree.get_selection().get_selected_rows()
        if len(it) == 1:
            p = it[0]
            i = mod.get_iter(p)
            if mod.iter_next(i) is not None:
                mod.swap(i, mod.iter_next(i))
                p.next()
                self.tree.scroll_to_cell(p)
                self.write_configuration()

    def on_fetchsvn_button_clicked(self, widget):
        (mod, it) = self.tree.get_selection().get_selected_rows()
        for i in it:
            name = mod[i][0]
            g = git(name, self)
            if g.check_svn():
                g.svnfetch()

    def on_rebasesvn_button_clicked(self, widget):
        (mod, it) = self.tree.get_selection().get_selected_rows()
        for i in it:
            name = mod[i][0]
            g = git(name, self)
            if g.check_svn():
                g.svnrebase()

    def on_dcommitsvn_button_clicked(self, widget):
        (mod, it) = self.tree.get_selection().get_selected_rows()
        for i in it:
            name = mod[i][0]
            g = git(name, self)
            if g.check_svn():
                g.svndcommit()

    def on_infosvn_button_clicked(self, widget):
        (mod, it) = self.tree.get_selection().get_selected_rows()
        for i in it:
            name = mod[i][0]
            g = git(name, self)
            if g.check_svn():
                g.svninfo()

    def on_pull_button_clicked(self, widget):
        (mod, it) = self.tree.get_selection().get_selected_rows()
        for i in it:
            name = mod[i][0]
            g = git(name, self)
            if not g.check_svn():
                g.pull()

    def on_push_button_clicked(self, widget):
        (mod, it) = self.tree.get_selection().get_selected_rows()
        for i in it:
            name = mod[i][0]
            g = git(name, self)
            if not g.check_svn():
                g.push()

    def on_status_button_clicked(self, widget):
        (mod, it) = self.tree.get_selection().get_selected_rows()
        for i in it:
            name = mod[i][0]
            g = git(name, self)
            g.status()

    def on_gc_button_clicked(self, widget):
        (mod, it) = self.tree.get_selection().get_selected_rows()
        for i in it:
            name = mod[i][0]
            g = git(name, self)
            g.gc()

    def on_clean_button_clicked(self, widget):
        (mod, it) = self.tree.get_selection().get_selected_rows()
        for i in it:
            name = mod[i][0]
            g = git(name, self)
            g.clean()

    def on_gitk_button_clicked(self, widget):
        (mod, it) = self.tree.get_selection().get_selected_rows()
        for i in it:
            name = mod[i][0]
            g = git(name, self)
            g.gitk()

    def on_gitkall_button_clicked(self, widget):
        (mod, it) = self.tree.get_selection().get_selected_rows()
        for i in it:
            name = mod[i][0]
            g = git(name, self)
            g.gitkall()

    def on_gitgui_button_clicked(self, widget):
        (mod, it) = self.tree.get_selection().get_selected_rows()
        for i in it:
            name = mod[i][0]
            g = git(name, self)
            g.gitgui()

    def on_term_button_clicked(self, widget):
        (mod, it) = self.tree.get_selection().get_selected_rows()
        for i in it:
            name = mod[i][0]
            g = git(name, self)
            g.term()

    def on_stash_button_clicked(self, widget):
        (mod, it) = self.tree.get_selection().get_selected_rows()
        for i in it:
            name = mod[i][0]
            g = git(name, self)
            g.stash()

    def on_stashpop_button_clicked(self, widget):
        (mod, it) = self.tree.get_selection().get_selected_rows()
        for i in it:
            name = mod[i][0]
            g = git(name, self)
            g.stash_pop()

    def read_configuration(self):
        config = configparser.ConfigParser()
        config.read(self.config_path)
        if config.has_section('Repos'):
            for repo in config['Repos']:
                name = config['Repos'][repo]
                if os.path.exists(name + "/.git"):
                    self._add_repo(name)

    def write_configuration(self):
        config = configparser.ConfigParser()
        config['Repos'] = {}
        i = 0
        for repo in self.repo_list:
            config['Repos']["repo{0}".format(i)] = repo[0]
            i += 1
        (directory, _) = os.path.split(self.config_path)
        os.makedirs(directory, exist_ok=True)
        with open(self.config_path, mode='w') as f:
            config.write(f)

    def _add_repo(self, path):
        g = git(path, self)
        cur, branch = g.get_branches()
        self.repo_list.append([path, "svn" if g.check_svn() else "git",
                               branch[cur], len(branch)])

    def show_error(self, message):
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR,
                                   Gtk.ButtonsType.CANCEL, "Error")
        dialog.format_secondary_text(message)
        dialog.run()
        dialog.destroy()

    def out_text(self, prefix, text):
        if prefix is not None:
            mark = self._get_mark()
            self.output.get_buffer().insert(self.output.get_buffer()
                                            .get_end_iter(), prefix + ":\n")
            self._apply_tag(mark, self.bold)
        self.output.get_buffer().insert(self.output.get_buffer()
                                        .get_end_iter(), text)
        self.output.scroll_to_mark(self.output.get_buffer()
                                   .create_mark(None, self.output.get_buffer()
                                                .get_end_iter()), 0.0, False,
                                   0, 0)

    def out_err(self, prefix, text):
        mark = self._get_mark()
        self.out_text(prefix, text)
        self._apply_tag(mark, self.red)

    def _get_mark(self):
        mark = None
        if self.output.get_buffer().get_char_count():
            it = self.output.get_buffer().get_end_iter()
            it.backward_char()
            mark = self.output.get_buffer().create_mark(None, it)
        return mark

    def _apply_tag(self, mark, tag):
        it = self.output.get_buffer().get_iter_at_mark(mark) if mark is not \
            None else self.output.get_buffer().get_start_iter()
        self.output.get_buffer().apply_tag(tag, it, self.output
                                           .get_buffer().get_end_iter())
