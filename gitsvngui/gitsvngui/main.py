'''
Created on Jun 8, 2012

@author: Martin Jakl
'''

import sys
from gi.repository import Gtk, GLib
from gitsvngui.window import MainWindow


Gtk.init_check(sys.argv)
GLib.set_prgname('Git SVN GUI')
win = MainWindow()
win.connect("delete-event", Gtk.main_quit)
win.set_wmclass("Git SVN GUI", "Git SVN GUI")
win.show_all()
Gtk.main()
