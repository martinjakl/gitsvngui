'''
Created on Jun 8, 2012

@author: Martin Jakl
'''

import subprocess
import os
import pty
from gi.repository import GObject


class git:
    pcount = 0
    gcount = 0
    statusline = None
    context = None

    def __init__(self, path, window):
        self.window = window
        self.path = path

    def _request(self, req):
        call = ["git", "-c", "color.ui=false"]
        mo, so = pty.openpty()
        me, se = pty.openpty()
        p = subprocess.Popen(call + req, stdout=so, stderr=se, cwd=self.path)
        GObject.io_add_watch(open(mo), GObject.IO_IN | GObject.IO_HUP,
                             self._io_watch, False, [])
        GObject.io_add_watch(open(me), GObject.IO_IN | GObject.IO_HUP,
                             self._io_watch, True, [])
        GObject.child_watch_add(p.pid, self._child_watch, (p, so, se))
        git.pcount += 1
        git._set_status()

    def _request_simple(self, req, nogit=False):
        call = ["git", "-c", "color.ui=false"]
        p = subprocess.Popen(req if nogit else call + req, cwd=self.path)
        GObject.child_watch_add(p.pid, self._child_watch_simple, p)
        git.gcount += 1
        git._set_status()

    def _io_watch(self, file, cond, red, first):
        try:
            s = file.readline()
            pref = None
            if len(first) == 0:
                first.append(1)
                pref = self.path
            if (red):
                self.window.out_err(pref, s)
            else:
                self.window.out_text(pref, s)
        except IOError:
            file.close()
            return False
        return True

    def _child_watch(self, pid, cond, data):
        p, so, se = data
        p.wait()
        os.close(so)
        os.close(se)
        git.pcount -= 1
        git._set_status()
        return True

    def _child_watch_simple(self, pid, cond, p):
        p.wait()
        git.gcount -= 1
        git._set_status()
        return True

    @staticmethod
    def _set_status():
        git.statusline.push(git.context, "Background processes: {0} GUI "
                            "processes {1}".format(git.pcount, git.gcount))

    def svnfetch(self):
        if self.check_svn():
            self._request(["svn", "fetch"])
        else:
            self.window.show_error("Not git svn repository")

    def svnrebase(self):
        if self.check_svn():
            self._request(["svn", "rebase"])
        else:
            self.window.show_error("Not git svn repository")

    def svndcommit(self):
        if self.check_svn():
            self._request(["svn", "dcommit"])
        else:
            self.window.show_error("Not git svn repository")

    def svninfo(self):
        if self.check_svn():
            self._request(["svn", "info"])
        else:
            self.window.show_error("Not git svn repository")

    def pull(self):
        if not self.check_svn():
            self._request(["pull"])
        else:
            self.window.show_error("Not plain git repository")

    def push(self):
        if not self.check_svn():
            self._request(["push"])
        else:
            self.window.show_error("Not plain git repository")

    def status(self):
        self._request(["status"])

    def gc(self):
        self._request(["gc"])
        if self.check_svn():
            self._request(["svn", "gc"])

    def clean(self):
        self._request(["clean", "-dxf"])

    def gitk(self):
        self._request_simple(["gitk"], True)

    def gitkall(self):
        self._request_simple(["gitk", "--all"], True)

    def gitgui(self):
        self._request_simple(["gui"])

    def term(self):
        self._request_simple(["gnome-terminal"], True)

    def check_svn(self):
        return os.path.exists(self.path + "/.git/svn")

    def get_branches(self):
        p = subprocess.Popen(["git", "-c", "color.ui=false", "branch"], stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE, cwd=self.path)
        (o, e) = p.communicate()
        if len(e):
            self.window.out_err(self.path, e.decode())
        branches = o.decode().split("\n")
        res = []
        curr = 0
        for b in branches:
            if len(b):
                s = b.split(" ")
                if s[0] == '*':
                    curr = len(res)
                res.append(s[len(s) - 1])
        return (curr, res)

    def checkout(self, branch):
        self._request(["checkout", branch])

    def stash(self):
        self._request(["stash"])

    def stash_pop(self):
        self._request(["stash", "pop"])

    @staticmethod
    def find_all_repos(window):
        p = subprocess.Popen(["find", os.environ["HOME"], "-name", ".git"],
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (o, e) = p.communicate()
        if len(e):
            window.out_err(None, e.decode())
        res = []
        paths = o.decode().split("\n")
        for p in paths:
            if len(p):
                (d, _) = os.path.split(p)
                res.append(d)
        return res
